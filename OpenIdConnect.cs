﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthTest
{
	public class OpenIdOptions
	{
		public string ClientId { get; set; }
		public string ClientSecret { get; set; }
		public string Authority { get; set; }
		public string ReplyUri { get; set; }
	}

	public class B2COptions : OpenIdOptions
	{
		public string SignUpSignInPolicyId { get; set; }

		public string PasswordResetPolicyId { get; set; }

		public string ProfileEditPolicyId { get; set; }

		public const string PolicyAuthenticationProperty = "Policy";
	}

	public static class AzureAdAuthenticationBuilderExtensions
	{
		public static AuthenticationBuilder AddB2C(this AuthenticationBuilder builder)
			=> builder.AddB2C(_ => { });

		public static AuthenticationBuilder AddB2C(this AuthenticationBuilder builder, Action<B2COptions> configureOptions)
		{
			builder.Services.Configure(configureOptions);
			builder.Services.AddSingleton<IConfigureOptions<OpenIdConnectOptions>, OpenIdConnectConfigureNamedOptions>();
			builder.AddOpenIdConnect();
			return builder;
		}

		private class OpenIdConnectConfigureNamedOptions : IConfigureNamedOptions<OpenIdConnectOptions>
		{
			private readonly B2COptions b2cOptions;

			public OpenIdConnectConfigureNamedOptions(IOptions<B2COptions> b2cOptions)
			{
				this.b2cOptions = b2cOptions.Value;
			}

			public void Configure(string name, OpenIdConnectOptions options)
			{
				options.ClientId = b2cOptions.ClientId;
				options.Authority = b2cOptions.Authority;
				options.UseTokenLifetime = true;
				options.CallbackPath = b2cOptions.ReplyUri;
				options.SignedOutCallbackPath = "/signout-oidc";
				options.ResponseType = "code";
				options.GetClaimsFromUserInfoEndpoint = true;
				options.TokenValidationParameters = new TokenValidationParameters() { NameClaimType = "name" };

				options.Events = new OpenIdConnectEvents()
				{
					OnRedirectToIdentityProvider = OnRedirectToIdentityProvider,
					OnRemoteFailure = OnRemoteFailure,
					OnAuthorizationCodeReceived = OnAuthorizationCodeReceived
				};
			}

			public void Configure(OpenIdConnectOptions options)
			{
				Configure(Options.DefaultName, options);
			}
			public Task OnRedirectToIdentityProvider(RedirectContext context)
			{
				var defaultPolicy = this.b2cOptions.SignUpSignInPolicyId;
				if (context.Properties.Items.TryGetValue(B2COptions.PolicyAuthenticationProperty, out var policy) &&
					!policy.Equals(defaultPolicy))
				{
					context.ProtocolMessage.Scope = OpenIdConnectScope.OpenIdProfile;
					context.ProtocolMessage.ResponseType = OpenIdConnectResponseType.IdToken;
					context.ProtocolMessage.IssuerAddress = context.ProtocolMessage.IssuerAddress.ToLower().Replace(defaultPolicy.ToLower(), policy.ToLower());
					context.Properties.Items.Remove(B2COptions.PolicyAuthenticationProperty);
				}

				return Task.FromResult(0);
			}

			public Task OnRemoteFailure(RemoteFailureContext context)
			{
				context.HandleResponse();
				// Handle the error code that Azure AD B2C throws when trying to reset a password from the login page 
				// because password reset is not supported by a "sign-up or sign-in policy"
				if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("AADB2C90118"))
				{
					// If the user clicked the reset password link, redirect to the reset password route
					context.Response.Redirect("/Identity/ResetPassword");
				}
				else if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("access_denied"))
				{
					context.Response.Redirect("/");
				}
				else
				{
					context.Response.Redirect("/Home/Error?message=" + WebUtility.UrlEncode(context.Failure.Message));
				}
				return Task.FromResult(0);
			}

			public async Task OnAuthorizationCodeReceived(AuthorizationCodeReceivedContext context)
			{
				var signedInUserID = context.Principal.FindFirst(ClaimTypes.NameIdentifier).Value;
				var userTokenCache = new TokenCache();
				var cca = new ConfidentialClientApplication(this.b2cOptions.ClientId, this.b2cOptions.Authority, this.b2cOptions.ReplyUri, new ClientCredential(this.b2cOptions.ClientSecret), userTokenCache, null);
				try
				{
					var result = await cca.AcquireTokenByAuthorizationCodeAsync(context.ProtocolMessage.Code, new string[] { string.Empty });
					context.HandleCodeRedemption(result.AccessToken, result.IdToken);
				}
				catch (Exception ex)
				{
					//TODO: Handle
					throw;
				}
			}
		}
	}
}
