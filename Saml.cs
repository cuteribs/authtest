﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Fastenal.Expo.Web.Helper
{
	public class Saml
	{
		private Settings settings;

		public Saml(Settings settings)
		{
			this.settings = settings;
		}

		public string GetSignInUrl()
		{
			var xnSamlp = (XNamespace)"urn:oasis:names:tc:SAML:2.0:protocol";
			var xnSaml = (XNamespace)"urn:oasis:names:tc:SAML:2.0:assertion";

			var xml = new XElement(xnSamlp + "AuthNRequest",
				new XAttribute(XNamespace.Xmlns + "samlp", xnSamlp),
				new XAttribute(XNamespace.Xmlns + "saml", xnSaml),
				new XAttribute("ID", "_" + Guid.NewGuid().ToString()),
				new XAttribute("Version", "2.0"),
				new XAttribute("IssueInstant", DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")),
				new XAttribute("ProtocolBinding", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"),
				new XAttribute("AssertionConsumerServiceURL", this.settings.ConsumerUrl),
				new XElement(xnSaml + "Issuer", this.settings.Issuer),
				new XElement(xnSamlp + "NameIDPolicy",
					new XAttribute("Format", "urn:oasis:names:tc:SAML:2.0:nameid-format:unspecified"),
					new XAttribute("AllowCreate", "true")
				),
				new XElement(xnSamlp + "RequestedAuthnContext",
					new XAttribute("Comparison", "exact"),
					new XElement(xnSaml + "AuthnContextClassRef",
						"urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport"
					)
				)
			);

			var bytes = Encoding.ASCII.GetBytes(xml.ToString());
			var saml = WebUtility.UrlEncode(Convert.ToBase64String(bytes));
			return $"{this.settings.AuthUrl}?SAMLRequest={saml}";
		}

		public UserIdentity GetUser(string response)
		{
			try
			{
				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(Encoding.ASCII.GetString(Convert.FromBase64String(response)));

				if (this.IsResponseValid(xmlDoc))
				{

					var manager = new XmlNamespaceManager(xmlDoc.NameTable);
					manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
					manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
					manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
					var nodes = xmlDoc.SelectNodes("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute", manager);
					string text = null;
					string employeeId = string.Empty,
						displayName = string.Empty,
						firstName = string.Empty,
						lastName = string.Empty,
						email = string.Empty,
						jobTitle = string.Empty,
						department = string.Empty,
						location = string.Empty;

					foreach (XmlNode node in nodes)
					{
						text = node.ChildNodes[0].InnerText;

						switch (node.Attributes["Name"].Value)
						{
							case "EmployeeID":
								employeeId = text;
								break;
							case "DisplayName":
								displayName = text;
								break;
							case "User.FirstName":
								firstName = text;
								break;
							case "User.LastName":
								lastName = text;
								break;
							case "User.email":
								email = text;
								break;
							case "Job Title":
								jobTitle = text;
								break;
							case "Department Code":
								department = text;
								break;
							case "Branch Code":
								location = text;
								break;
						}
					}

					var user = UserIdentity.CreateEmployeeIdentity(email, employeeId, displayName, firstName, lastName, jobTitle, department, location);
					return user;
				}
			}
			catch { }

			return null;
		}

		public string GetSignOutUrl(string email)
		{
			var xnSamlp = (XNamespace)"urn:oasis:names:tc:SAML:2.0:protocol";
			var xnSaml = (XNamespace)"urn:oasis:names:tc:SAML:2.0:assertion";

			var xml = new XElement(xnSamlp + "LogoutRequest",
				new XAttribute(XNamespace.Xmlns + "samlp", xnSamlp),
				new XAttribute(XNamespace.Xmlns + "saml", xnSaml),
				new XAttribute("ID", "_" + Guid.NewGuid().ToString()),
				new XAttribute("Version", "2.0"),
				new XAttribute("IssueInstant", DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")),
				new XAttribute("Destination", this.settings.SignOutConsumerUrl),
				new XElement(xnSaml + "Issuer", this.settings.Issuer),
				new XElement(xnSamlp + "NameID", email,
					new XAttribute("SPNameQualifier", this.settings.Issuer),
					new XAttribute("Format", "urn:oasis:names:tc:SAML:2.0:nameid-format:transient")
				)
			);

			var bytes = ASCIIEncoding.ASCII.GetBytes(xml.ToString());
			var saml = WebUtility.UrlEncode(Convert.ToBase64String(bytes));
			return $"{this.settings.SignOutUrl}?SAMLRequest={saml}";
		}

		private bool IsResponseValid(XmlDocument xmlDoc)
		{
			var manager = new XmlNamespaceManager(xmlDoc.NameTable);
			manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
			manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
			manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
			var nodes = xmlDoc.SelectNodes("//ds:Signature", manager);

			var signedXml = new SignedXml(xmlDoc);
			signedXml.LoadXml((XmlElement)nodes[0]);

			var cert = new X509Certificate2(Encoding.ASCII.GetBytes(this.settings.Certificate));

			if (!signedXml.CheckSignature(cert, true))
			{
				return false;
			}

			var node = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:Conditions", manager);
			var notBefore = DateTime.Parse(node.Attributes["NotBefore"].Value);
			var notOnOrAfter = DateTime.Parse(node.Attributes["NotOnOrAfter"].Value);

			return notBefore <= DateTime.Now || notOnOrAfter > DateTime.Now;
		}
		
		public class Settings
		{
			public string Certificate { get; set; }
			public string AuthUrl { get; set; }
			public string ConsumerUrl { get; set; }
			public string SignOutUrl { get; set; }
			public string SignOutConsumerUrl { get; set; }
			public string Issuer { get; set; }
		}
	}
}