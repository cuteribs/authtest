﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Fastenal.Expo.Web.Helper
{
	public class UserIdentity : ClaimsIdentity
	{
		public string Email { get; set; }
		public string EmployeeId { get; set; }
		public string DisplayName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string JobTitle { get; set; }
		public string Department { get; set; }
		public string Location { get; set; }
		public string Role { get; set; }
		public Dictionary<int, int> Attendings { get; set; }

		private UserIdentity() : base() { }

		private UserIdentity(IIdentity identity) : base(identity) { }

		private UserIdentity(string authenticationType) : base(authenticationType) { }

		public static UserIdentity CreateEmployeeIdentity(string email, string employeeId, string displayName, string firstName, string lastName, string jobTitle, string department, string location, string role = "E", Dictionary<int, int> attendings = null)
		{
			var user = new UserIdentity(CookieAuthenticationDefaults.AuthenticationScheme)
			{
				Email = email,
				EmployeeId = employeeId,
				DisplayName = displayName,
				FirstName = firstName,
				LastName = lastName,
				JobTitle = jobTitle,
				Department = department,
				Location = location,
				Role = role,
				Attendings = attendings
			};

			user.AddClaim(new Claim(ClaimTypes.Name, email));
			user.AddClaim(new Claim(nameof(user.Email), email));
			user.AddClaim(new Claim(nameof(user.EmployeeId), employeeId));
			user.AddClaim(new Claim(nameof(user.DisplayName), displayName));
			user.AddClaim(new Claim(nameof(user.FirstName), firstName));
			user.AddClaim(new Claim(nameof(user.LastName), lastName));
			user.AddClaim(new Claim(nameof(user.JobTitle), jobTitle));
			user.AddClaim(new Claim(nameof(user.Department), department));
			user.AddClaim(new Claim(nameof(user.Location), location));
			user.AddClaim(new Claim(ClaimTypes.Role, role));
			user.AddClaim(new Claim(nameof(user.Attendings), JsonConvert.SerializeObject(attendings)));

			return user;
		}

		public static UserIdentity CreateUserIdentity(string email, string displayName, string firstName, string lastName, string jobTitle, string role = "C", Dictionary<int, int> attendings = null)
		{
			var user = new UserIdentity(CookieAuthenticationDefaults.AuthenticationScheme)
			{
				Email = email,
				DisplayName = displayName,
				FirstName = firstName,
				LastName = lastName,
				JobTitle = jobTitle,
				Role = role,
				Attendings = attendings
			};

			user.AddClaim(new Claim(ClaimTypes.Name, email));
			user.AddClaim(new Claim(nameof(user.Email), email));
			user.AddClaim(new Claim(nameof(user.DisplayName), displayName));
			user.AddClaim(new Claim(nameof(user.FirstName), firstName));
			user.AddClaim(new Claim(nameof(user.LastName), lastName));
			user.AddClaim(new Claim(nameof(user.JobTitle), jobTitle));
			user.AddClaim(new Claim(ClaimTypes.Role, role));
			user.AddClaim(new Claim(nameof(user.Attendings), JsonConvert.SerializeObject(attendings)));

			return user;
		}

		public static UserIdentity CreateUserIdentity(IIdentity identity)
		{
			var user = new UserIdentity(identity);
			var claim = user.FindFirst(ClaimTypes.Name);

			if (claim != null)
			{
				user.Email = user.FindFirst(nameof(user.Email)).Value;
				user.EmployeeId = user.FindFirst(nameof(user.EmployeeId))?.Value;
				user.DisplayName = user.FindFirst(nameof(user.DisplayName)).Value;
				user.FirstName = user.FindFirst(nameof(user.FirstName)).Value;
				user.LastName = user.FindFirst(nameof(user.LastName)).Value;
				user.JobTitle = user.FindFirst(nameof(user.JobTitle)).Value;
				user.Department = user.FindFirst(nameof(user.Department))?.Value;
				user.Location = user.FindFirst(nameof(user.Location))?.Value;
				user.Role = user.FindFirst(ClaimTypes.Role).Value;
				user.Attendings = JsonConvert.DeserializeObject<Dictionary<int, int>>(user.FindFirst(nameof(user.Attendings)).Value);
			}

			return user;
		}

		public void SetRole(string role)
		{
			this.Role = role;
			this.RemoveClaim(this.Claims.First(c => c.Type == ClaimTypes.Role));
			this.AddClaim(new Claim(ClaimTypes.Role, role));
		}
	}
}
