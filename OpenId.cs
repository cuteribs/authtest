﻿using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;

namespace Fastenal.Expo.Web.Helper
{
	public class OpenId
	{
		private readonly Settings settings;

		public OpenId(Settings settings)
		{
			this.settings = settings;
		}

		public async Task<string> GetSignInUrl(string email = null)
		{
			var cm = new ConfigurationManager<OpenIdConnectConfiguration>($"{this.settings.ConfigUrl}?p={this.settings.SignUpSignInPolicyId}", new OpenIdConnectConfigurationRetriever());
			var config = await cm.GetConfigurationAsync();
			var url = $"{config.AuthorizationEndpoint}&client_id={this.settings.ClientId}&redirect_uri={WebUtility.UrlEncode(this.settings.ReplyUri)}&nonce={Guid.NewGuid().ToString()}&scope=openid&response_mode=form_post&response_type=id_token&prompt=login&login_hint={email}";
			return url;
		}

		public async Task<string> GetPasswordResetUrl(string email)
		{
			var cm = new ConfigurationManager<OpenIdConnectConfiguration>($"{this.settings.ConfigUrl}?p={this.settings.PasswordResetPolicyId}", new OpenIdConnectConfigurationRetriever());
			var config = await cm.GetConfigurationAsync();
			var url = $"{config.AuthorizationEndpoint}&client_id={this.settings.ClientId}&redirect_uri={WebUtility.UrlEncode(this.settings.ReplyUri)}&nonce={Guid.NewGuid().ToString()}&scope=openid&response_mode=form_post&response_type=id_token&prompt=login&login_hint={email}";
			return url;
		}

		public async Task<string> GetProfileEditUrl(string email)
		{
			var cm = new ConfigurationManager<OpenIdConnectConfiguration>($"{this.settings.ConfigUrl}?p={this.settings.ProfileEditPolicyId}", new OpenIdConnectConfigurationRetriever());
			var config = await cm.GetConfigurationAsync();
			var url = $"{config.AuthorizationEndpoint}&client_id={this.settings.ClientId}&redirect_uri={WebUtility.UrlEncode(this.settings.ReplyUri)}&nonce={Guid.NewGuid().ToString()}&scope=openid&response_mode=form_post&response_type=id_token&prompt=login&login_hint={email}";
			return url;
		}

		public async Task<UserIdentity> ValidateToken(string idToken)
		{
			var cm = new ConfigurationManager<OpenIdConnectConfiguration>($"{this.settings.ConfigUrl}?p={this.settings.SignUpSignInPolicyId}", new OpenIdConnectConfigurationRetriever());
			var config = await cm.GetConfigurationAsync();

			var handler = new JwtSecurityTokenHandler();
			var parameters = new TokenValidationParameters()
			{
				ValidIssuer = config.Issuer,
				ValidAudience = this.settings.ClientId,
				IssuerSigningKeys = config.SigningKeys
			};

			var token = handler.ValidateToken(idToken, parameters, out _);
			var email = token.Claims.FirstOrDefault(c => c.Type == "emails").Value;
			var displayName = token.Claims.FirstOrDefault(c => c.Type == "name").Value;
			var firstName = token.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName).Value;
			var lastName = token.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname).Value;
			var jobTitle = token.Claims.FirstOrDefault(c => c.Type == "jobTitle").Value;
			var user = UserIdentity.CreateUserIdentity(email, displayName, firstName, lastName, jobTitle);
			return user;
		}

		public class Settings
		{
			public string ConfigUrl { get; set; }

			public string ClientId { get; set; }

			public string ReplyUri { get; set; }

			public string SignUpSignInPolicyId { get; set; }

			public string PasswordResetPolicyId { get; set; }

			public string ProfileEditPolicyId { get; set; }
		}
	}

}
