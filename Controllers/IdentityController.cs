﻿using Fastenal.Expo.Web.Helper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthTest.Controllers
{
	public class IdentityController : Controller
	{
		private IConfiguration configuration;

		protected IConfiguration Configuration => this.configuration ?? (this.configuration = this.HttpContext.RequestServices.GetService<IConfiguration>());

		private OpenId openId;

		private OpenId OpenId
		{
			get
			{
				if (this.openId == null)
				{
					var settings = new OpenId.Settings();
					this.Configuration.GetSection("OpenId:B2C").Bind(settings);
					this.openId = new OpenId(settings);
				}

				return this.openId;
			}
		}

		private Saml saml;

		private Saml Saml
		{
			get
			{
				if (this.saml == null)
				{
					var settings = new Saml.Settings();
					this.Configuration.GetSection("OneLoginSaml").Bind(settings);
					this.saml = new Saml(settings);
				}

				return this.saml;
			}
		}

		private UserIdentity currentUser;

		private UserIdentity CurrentUser => this.currentUser ?? (this.currentUser = UserIdentity.CreateUserIdentity(this.HttpContext.User.Identity));

		public IdentityController()
		{
		}

		public async Task<IActionResult> SignIn(string email = null)
		{
			if (string.IsNullOrWhiteSpace(email))
			{
				return this.Redirect("/");
			}

			if (email.ToLower().IndexOf("@fastenal.com") > 0)
			{
				return this.Redirect(this.Saml.GetSignInUrl());
			}

			return this.Redirect(await this.OpenId.GetSignInUrl(email));
		}

		public async Task<IActionResult> SignIn2()
		{
			var settings = new OpenId.Settings();
			this.Configuration.GetSection("OpenId:OneLogin").Bind(settings);
			var openId = new OpenId(settings);
			return this.Redirect(await openId.GetSignInUrl());
		}

		[HttpPost, Route("user/signed-in")]
		public async Task<IActionResult> SignedIn()
		{
			UserIdentity user;

			if (this.Request.Form.ContainsKey("SAMLResponse"))
			{
				user = this.Saml.GetUser(this.Request.Form["SAMLResponse"]);
			}
			else if (this.Request.Form.ContainsKey("id_token"))
			{
				user = await this.OpenId.ValidateToken(this.Request.Form["id_token"]);
			}
			else
			{
				return this.RedirectToAction("Index", "Home");
			}

			var cp = new ClaimsPrincipal(user);
			await this.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, cp, new AuthenticationProperties { IsPersistent = false });
			// Cookie claims needs a page load action to work
			return this.RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public async Task<IActionResult> ResetPassword()
		{
			if (this.CurrentUser.Role == "E")
			{
				return this.RedirectToAction("Index", "Home");
			}

			var url = await this.OpenId.GetPasswordResetUrl(this.CurrentUser.Email);
			return this.Redirect(url);
		}

		[HttpGet]
		public async Task<IActionResult> EditProfile()
		{
			if (this.CurrentUser.Role == "E")
			{
				return this.RedirectToAction("Index", "Home");
			}

			var url = await this.OpenId.GetProfileEditUrl(this.CurrentUser.Email);
			return this.Redirect(url);
		}

		[HttpGet]
		public async Task<IActionResult> SignOut()
		{
			await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

			if (this.CurrentUser.Role == "E")
			{
				var url = this.Saml.GetSignOutUrl(this.CurrentUser.Email);
				return this.Redirect(url);
			}

			return this.RedirectToAction("Index", "Home");
		}
	}
}