﻿using AuthTest.Models;
using Fastenal.Expo.Web.Helper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthTest.Controllers
{
	public class HomeController : Controller
	{
		private IConfiguration configuration;

		protected IConfiguration Configuration => this.configuration ?? (this.configuration = this.HttpContext.RequestServices.GetService<IConfiguration>());

		private Saml.Settings oneLoginSettings;

		protected Saml.Settings OneLoginSettings
		{
			get
			{
				if (this.oneLoginSettings == null)
				{
					this.oneLoginSettings = new Saml.Settings();
					this.Configuration.GetSection("OneLoginSaml").Bind(this.oneLoginSettings);
				}

				return this.oneLoginSettings;
			}
		}

		private OpenId.Settings openIdSettings;

		protected OpenId.Settings OpenIdSettings
		{
			get
			{
				if (this.openIdSettings == null)
				{
					this.openIdSettings = new OpenId.Settings();
					this.Configuration.GetSection("OpenId:B2C").Bind(this.openIdSettings);
				}

				return this.openIdSettings;
			}
		}

		[HttpGet]
		public async Task<IActionResult> Index()
		{
			this.ViewBag.OneLoginUrl = new Saml(this.OneLoginSettings).GetSignInUrl();
			this.ViewBag.B2CUrl = await new OpenId(this.OpenIdSettings).GetSignInUrl("eric@test.com");

			return this.View();
		}

		public IActionResult About()
		{
			ViewData["Message"] = "Your application description page.";

			return View();
		}

		public IActionResult Contact()
		{
			ViewData["Message"] = "Your contact page.";

			return View();
		}

		public IActionResult SaveForm()
		{
			return Json(null);
		}

		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
